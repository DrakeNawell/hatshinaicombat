#include <iostream>
#include <algorithm>
#include <vector>

#include <cstdlib>
#include <ctime>

#include "Characters/Ellie.hpp"
#include "Characters/Etan.hpp"
#include "Characters/Aelia.hpp"
#include "Enemy.hpp"
#include "Battle.hpp"

using namespace Game;

/*int main() {
    std::vector<Character*> chars;

    chars.push_back(new Character("Etan", "Char1", "Gunner"));
    chars.push_back(new Character("Ellie", "Char1", "Tank"));
    Battle b(chars, 2);
    b = b;
}*/

int main() {
    std::vector<Character*> characters;
    std::vector<Enemy*> enemies;

    bool running;

    std::string choice;

    running = true;
    choice = "";

    std::cout << "Welcome to Hateshinai Combat System Demo" << std::endl;
    std::cout << "This demo version is runned through console environment" << std::endl << "Final version will include graphics" << std::endl;
    std::cout << std::endl;
    std::cout << "Initializing characters..." << std::endl;
    std::cout << std::endl;
    characters.push_back(new Ellie());
    characters.push_back(new Etan());
    characters.push_back(new Aelia());
    //characters.push_back(new Character("Mai Wayne", "Supportive Character", "Gunner"));
    std::cout << std::endl;
    while (running) {
        int nbEnemies;

        srand(time(nullptr));
        std::cout << "Initializing enemies..." << std::endl;
        std::cout << std::endl;
        nbEnemies = (rand() % 4) + 1;
        for (int i = 0; i < nbEnemies; i++) {
            enemies.push_back(new Enemy("Enemy", "Enemy"));
        }
        std::cout << std::endl;
        std::cout << nbEnemies << " enemies have spawned" << std::endl;
        std::cout << std::endl;
        while (enemies.size() != 0) {
            std::cout << "What will you do ?" << std::endl;
            std::cout << std::endl;
            std::cout << "1. Attack\t\t\t2. List Enemies" << std::endl;
            std::cout << "3. List Characters\t\t4. Escape" << std::endl;
            std::cout << std::endl;
            std::cout << "Your choice : ";
            std::cin >> choice;
            try {
                int convertedChoice;

                convertedChoice = stoi(choice);
                switch(convertedChoice) {
                    case 1: {
                        std::vector<Character*>::iterator it;

                        std::cout << "You decided to attack" << std::endl;
                        std::cout << std::endl;
                        for (it = characters.begin(); it != characters.end(); it++) {
                            Entity *toAttack;
                            std::string choiceAttack;
                            unsigned int c, cAttack;
                            bool enemyChosen = false, attackChosen = false;

                            c = 0;
                            while (!enemyChosen && !attackChosen && enemies.size() > 0) {
                                std::vector<Enemy*>::iterator itEnemy;
                                int i;

                                i = 0;
                                std::cout << (*it)->GetName() << ": Which enemy do you want to attack ?" << std::endl;
                                std::cout << std::endl;
                                for (itEnemy = enemies.begin(); itEnemy != enemies.end(); itEnemy++) {
                                    i++;
                                    std::cout << i << ". " << (*itEnemy)->GetName() << " (" << (*itEnemy)->GetHP() << "HP left)\t" << std::endl;
                                    if ((i % 5) == 0 && i != 0) {
                                        std::cout << std::endl;
                                    }
                                }
                                std::cout << std::endl;
                                std::cout << "Your choice : ";
                                std::cin >> choice;
                                std::cout << std::endl;
                                c = stoi(choice) - 1;
                                if (c < enemies.size()) {
                                    enemyChosen = true;
                                    toAttack = enemies[c];
                                    std::cout << std::endl;
                                    while (!attackChosen) {
                                        std::cout << (*it)->GetName() << ": Which attack will I use ?" << std::endl;
                                        std::cout << std::endl;
                                        if ((*it)->GetJob()->GetSpells().size() > 0) {
                                            for (unsigned int i = 0; i < (*it)->GetJob()->GetSpells().size(); i++) {
                                                std::cout << (i + 1) << ". " << (*it)->GetJob()->GetSpells()[i]->GetName() << "\t\t";
                                                if ((i % 2) == 0 && i != 0) {
                                                    std::cout << std::endl;
                                                }
                                            }
                                            std::cout << std::endl;
                                            std::cout << "Your choice : ";
                                            std::cin >> choiceAttack;
                                            cAttack = stoi(choiceAttack);
                                            if (cAttack <= (*it)->GetJob()->GetSpells().size()) {
                                                attackChosen = true;
                                            }
                                        } else {
                                            attackChosen = true;
                                        }
                                    }
                                    std::cout << std::endl;
                                    if ((*it)->GetJob()->GetSpells().size() > 0) {
                                        (*it)->GetJob()->GetSpells()[cAttack - 1]->Use(toAttack, (*it));
                                    }
                                } else {
                                    std::cout << "Please enter a valid option" << std::endl;
                                    std::cout << std::endl;
                                }
                            }
                            if (enemies.size() > 0) {
                                (*it)->Attack(enemies[c]);
                                std::cout << std::endl;
                                std::cout << (*it)->GetName() << " attacked " << enemies[c]->GetName() << std::endl;
                                std::cout << std::endl;
                                if (enemies[c]->GetHP() <= 0) {
                                    enemies.erase(enemies.begin() + c);
                                }
                            }
                        }
                        break;
                    }
                    case 2: {
                        std::vector<Enemy*>::const_iterator it;

                        for (it = enemies.cbegin(); it != enemies.cend(); it++) {
                            std::cout << (*it)->ToString() << std::endl;
                        }
                        break;
                    }
                    case 3: {
                        std::vector<Character*>::const_iterator it;
                        
                        for (it = characters.cbegin(); it != characters.cend(); it++) {
                            std::cout << (*it)->ToString() << std::endl;
                        }
                        break;
                    }
                    case 4:
                        int percentage;

                        srand(time(nullptr));
                        percentage = rand() % 100;
                        std::cout << std::endl;
                        if (percentage < 75) {
                            std::cout << "You escaped the fight" << std::endl;
                            enemies.clear();
                        } else {
                            std::cout << "You couldn't escape from the fight" << std::endl; 
                        }
                        std::cout << std::endl;
                        break;
                    default:
                        std::cout << "Please enter a number between 1 and 4" << std::endl;
                        std::cout << std::endl;
                        break;
                }
            } catch (std::exception e) {
                std::transform(choice.begin(), choice.end(), choice.begin(), ::tolower);
                if (choice.compare("quit") == 0) {
                    running = false;
                } else {
                    std::cout << "Please enter a valid information" << std::endl;
                    std::cout << std::endl;
                }
            }
        }
    }
    return (0);
}