SRCDIR=		./src/
INCDIR=		./include/

SRC=		$(SRCDIR)Character.cpp \
			$(SRCDIR)Enemy.cpp \
			$(SRCDIR)Entity.cpp \
			$(SRCDIR)Job.cpp \
			$(SRCDIR)Spell.cpp \
			$(SRCDIR)Battle.cpp \
			$(SRCDIR)Characters/Ellie.cpp \
			$(SRCDIR)Characters/Etan.cpp \
			$(SRCDIR)Characters/Aelia.cpp \
			$(SRCDIR)Jobs/Analyst.cpp \
			$(SRCDIR)Jobs/Arcanist.cpp \
			$(SRCDIR)Jobs/Carrier.cpp \
			$(SRCDIR)Jobs/Devastator.cpp \
			$(SRCDIR)Jobs/Gunner.cpp \
			$(SRCDIR)Jobs/Healer.cpp \
			$(SRCDIR)Jobs/Mage.cpp \
			$(SRCDIR)Jobs/Restrainer.cpp \
			$(SRCDIR)Jobs/Support.cpp \
			$(SRCDIR)Jobs/Tank.cpp \
			$(SRCDIR)Jobs/Warrior.cpp \
			$(SRCDIR)Status.cpp \
			Main.cpp

OBJS=		$(SRC:.cpp=.o)

CXX=		g++

CXXFLAGS=	-Wall -Wextra -Werror -g3
CXXFLAGS+=	-I $(INCDIR)

LDFLAGS=	

NAME=		HateshinaiCombatSystem

all:		$(NAME)

$(NAME):	$(OBJS)
			$(CXX) -o $(NAME) $(OBJS) $(LDFLAGS)

clean:
			rm -rf $(OBJS)

fclean:		clean
			rm -rf $(NAME)

re:			fclean all