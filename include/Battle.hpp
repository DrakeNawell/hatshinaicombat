#pragma once

#include <list>

#include "Character.hpp"
#include "Enemy.hpp"

namespace Game {

    class Battle {

        private:

            std::list<Entity*> _turns;

            std::vector<Character*> _characters;
            std::vector<Enemy*> _enemies;

            std::map<std::string const, Entity*> _lineAlly1;
            std::map<std::string const, Entity*> _lineAlly2;
            std::map<std::string const, Entity*> _lineAlly3;

            std::map<std::string const, Entity*> _lineEnemy1;
            std::map<std::string const, Entity*> _lineEnemy2;
            std::map<std::string const, Entity*> _lineEnemy3;

            void InitializeBattle(unsigned int nbEnemies);

        public:

            Battle(std::vector<Character*> const& characters, unsigned int nbEnemies);
            ~Battle();

            void ChooseAction();

            void ChooseAttack();
            void ChooseDefense();
            void ChooseSkill();
            void ChooseObject();
            void Escape();
            
            void ChooseTarget();

    };

};