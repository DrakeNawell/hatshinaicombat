#pragma once

#include "Entity.hpp"

#include "Jobs/Analyst.hpp"
#include "Jobs/Arcanist.hpp"
#include "Jobs/Carrier.hpp"
#include "Jobs/Devastator.hpp"
#include "Jobs/Gunner.hpp"
#include "Jobs/Healer.hpp"
#include "Jobs/Mage.hpp"
#include "Jobs/Restrainer.hpp"
#include "Jobs/Support.hpp"
#include "Jobs/Tank.hpp"
#include "Jobs/Warrior.hpp"

namespace Game {
    
    class Character : public Entity {

        private:

            std::vector<Job*> _characterJobs;
            int _selectedJob;

        protected:

            std::vector<Job*>& GetJobs();

        public:

            Character(std::string const& nom, std::string const& bio, std::string const& job);
            virtual ~Character();

            Job *GetJob() const;
            void SwitchJob(std::string const& newJob);

            virtual void Attack(Entity *toAttack);
            virtual void TakeDamage(unsigned int damage);

            virtual std::string const ToString();

    };

};