#pragma once

#include "Character.hpp"

namespace Game {

    class Aelia : public Character {

        private:

            void InitializeJobs();
            void InitializeSpells();

        public:

            Aelia();
            virtual ~Aelia();

    };

};