#pragma once

#include "Character.hpp"

namespace Game {

    class Ellie : public Character {

        private:

            void InitializeJobs();
            void InitializeSpells();

        public:

            Ellie();
            virtual ~Ellie();

    };

};