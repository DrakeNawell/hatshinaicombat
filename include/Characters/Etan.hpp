#pragma once

#include "Character.hpp"

namespace Game {

    class Etan : public Character {

        private:

            void InitializeJobs();
            void InitializeSpells();

        public:

            Etan();
            virtual ~Etan();

    };

};