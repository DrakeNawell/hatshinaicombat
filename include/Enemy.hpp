#pragma once

#include "Entity.hpp"

namespace Game {

    class Enemy : public Entity {

        private:

            bool _isAnalyzed;

        public:

            Enemy(std::string const& name, std::string const& bio);
            virtual ~Enemy();

            virtual void Attack(Entity *toAttack);
            virtual void TakeDamage(unsigned int damage);

            void SetIsAnalyzed(bool newIsAnalyzed);

            virtual std::string const ToString();

    };

};