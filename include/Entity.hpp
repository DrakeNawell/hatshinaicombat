#pragma once

#include <iostream>
#include <string>
#include <map>

#include "Status.hpp"

namespace Game {

    class Entity {

        private:

            std::string _name;
            std::string _bio;
            std::map<Status, char> _status;

            unsigned int _line;
            std::string _idPos;

            int _hp;
            int _ep;
            int _lvl;
            int _exp;

            int _str;
            int _pwr;
            int _wsd;
            int _skl;
            int _hlt;
            int _spd;
            int _cst;
            int _spt;
            int _eng;

        public:

            void SetName(std::string const& newName);
            void SetBio(std::string const& newBio);
            void SetHP(int newHP);
            void SetEP(int newEP);
            void SetLVL(int newLVL);
            void SetEXP(int newEXP);

            void SetSTR(int newSTR);
            void SetPWR(int newPWR);
            void SetWSD(int newWSD);
            void SetSKL(int newSKL);
            void SetHLT(int newHLT);
            void SetSPD(int newSPD);
            void SetCST(int newCST);
            void SetSPT(int newSPT);
            void SetENG(int newENG);

            void SetLine(int line);
            void SetIDPosition(std::string const& newID);

            std::map<Status, char> const& GetStatus() const;

        public:

            Entity(std::string const& name, std::string const& bio, int lvl = 51);
            virtual ~Entity();

            std::string const& GetName() const;
            std::string const& GetBio() const;
            int GetHP() const;
            int GetEP() const;
            int GetLVL() const;
            int GetEXP() const;
            
            int GetSTR() const;
            int GetPWR() const;
            int GetWSD() const;
            int GetSKL() const;
            int GetHLT() const;
            int GetSPD() const;
            int GetCST() const;
            int GetSPT() const;
            int GetENG() const;

            int GetLine() const;
            std::string const& GetIDPosition() const;

            void ApplyStatus(Status toApply, StatusType type);
            void DismissStatus(Status toDismiss);
            unsigned int GetStatusStack(Status toGet);

            virtual void Attack(Entity *toAttack) = 0;
            virtual void TakeDamage(unsigned int damage) = 0;

            virtual std::string const ToString() = 0;

    };

};