#pragma once

#include <string>
#include <vector>

#include "Spell.hpp"

/**
 * ---- ROLE ----
 * Mage
 * Ravageur
 * Guerrier
 * Artilleur
 * Entraveur
 * Tank
 * Soigneur
 * Support
 * 
 * Analyste -> Mage, Tank, Support, Entraveur
 * Soutien -> Support, Entraveur, Mage
 * Arcaniste -> Mage, Artilleur
 **/

namespace Game {

    class Job {

        private:

            std::string _jobName;
            std::string _jobRealName;
            std::vector<Spell*> _jobSpells;

            int _str;
            int _pwr;
            int _wsd;
            int _skl;
            int _hlt;
            int _spd;
            int _cst;
            int _spt;
            int _eng;

            int _resWTR;
            int _resFIR;
            int _resGRD;
            int _resAIR;
            int _resTND;
            int _resICE;
            int _resNTR;
            int _resMCH;
            int _resLGT;
            int _resDRK;
            int _resPHS;
            int _resMGK;
            int _resWAV;
            int _resSND;

        protected:

            void SetSTR(int newSTR);
            void SetPWR(int newPWR);
            void SetWSD(int newWSD);
            void SetSKL(int newSKL);
            void SetHLT(int newHLT);
            void SetSPD(int newSPD);
            void SetCST(int newCST);
            void SetSPT(int newSPT);
            void SetENG(int newENG);

            void SetResWTR(int newResWTR);
            void SetResFIR(int newResFIR);
            void SetResGRD(int newResGRD);
            void SetResAIR(int newResAIR);
            void SetResTND(int newResTND);
            void SetResICE(int newResICE);
            void SetResNTR(int newResNTR);
            void SetResMCH(int newResMCH);
            void SetResLGT(int newResLGT);
            void SetResDRK(int newResDRK);
            void SetResPHS(int newResPHS);
            void SetResMGK(int newResMGK);
            void SetResWAV(int newResWAV);
            void SetResSND(int newResSND);

        public:

            Job(std::string const& jobName, std::string const& realName);
            Job(std::string const& jobName, std::string const& realName, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng);
            Job(std::string const& jobName, std::string const& realName, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng,
                int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND);
            virtual ~Job();

            std::string const& GetJobName() const;
            std::string const& GetJobRealName() const;

            int GetSTR() const;
            int GetPWR() const;
            int GetWSD() const;
            int GetSKL() const;
            int GetHLT() const;
            int GetSPD() const;
            int GetCST() const;
            int GetSPT() const;
            int GetENG() const;

            int GetResWTR() const;
            int GetResFIR() const;
            int GetResGRD() const;
            int GetResAIR() const;
            int GetResTND() const;
            int GetResICE() const;
            int GetResNTR() const;
            int GetResMCH() const;
            int GetResLGT() const;
            int GetResDRK() const;
            int GetResPHS() const;
            int GetResMGK() const;
            int GetResWAV() const;
            int GetResSND() const;

            std::vector<Spell*> const& GetSpells() const;

            void InitializeSpells(std::string const& name);

    };

};