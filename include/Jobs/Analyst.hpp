#pragma once

#include "Job.hpp"

namespace Game {

    class Analyst : public Job {

        private:

        public:
        
            Analyst(std::string const& jobName);
            Analyst(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng);
            Analyst(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng,
                int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND);
            virtual ~Analyst();

    };

};