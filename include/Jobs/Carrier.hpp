#pragma once

#include "Job.hpp"

namespace Game {

    class Carrier : public Job {

        private:

        public:

            Carrier(std::string const& jobName);
            Carrier(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng);
            Carrier(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng,
                int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND);

            virtual ~Carrier();

    };

};