#pragma once

#include "Job.hpp"

namespace Game {

    class Restrainer : public Job {

        private:

        public:

            Restrainer(std::string const& jobName);
            Restrainer(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng);
            Restrainer(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng,
                int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND);

            virtual ~Restrainer();

    };

};