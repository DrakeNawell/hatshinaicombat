#pragma once

#include "Job.hpp"

namespace Game {

    class Warrior : public Job {

        private:

        public:

            Warrior(std::string const& jobName);
            Warrior(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng);
            Warrior(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng,
                int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND);

            virtual ~Warrior();

    };

};