#pragma once

#include <string>
#include <random>

#include "Status.hpp"
#include "Entity.hpp"

namespace Game {

    class Spell {

        private:

            std::random_device _rd;

            
            std::string _name;
            unsigned int _energyUsed;
            unsigned int _pwrSpellMin;
            unsigned int _pwrSpellMax;
            double _criticalChances;
            unsigned int _precision;


        protected:

            unsigned int GetChance(int min, int max);

            void SetName(std::string const& newName);
            void SetEnergyUsed(int newEnergyUsed);
            void SetPWRSpellMin(int newPWRSpellMin);
            void SetPWRSpellMax(int newPWRSpellMax);
            void SetCriticalChances(int newCriticalChances);
            void SetPrecision(int newPrecision);

        public:

            Spell(std::string const& name);
            virtual ~Spell();

            std::string const& GetName() const;
            unsigned int GetEnergyUsed() const;
            unsigned int GetPWRSpellMin() const;
            unsigned int GetPWRSpellMax() const;
            double GetCriticalChances() const;
            unsigned int GetPrecision() const;

            virtual void Use(Entity* to, Entity* from) = 0;

    };

};