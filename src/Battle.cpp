#include "Battle.hpp"

using namespace Game;

Battle::Battle(std::vector<Character*> const& characters, unsigned int nbEnnemies) {
    this->_characters = characters;

    this->_lineAlly1 = {
        {"1-1", nullptr},
        {"1-2", nullptr},
        {"1-3", nullptr},
        {"2-1", nullptr},
        {"2-2", nullptr},
        {"2-3", nullptr},
        {"3-1", nullptr},
        {"3-2", nullptr},
        {"3-3", nullptr}
    };


    this->_lineAlly2 = {
        {"1-1", nullptr},
        {"1-2", nullptr},
        {"1-3", nullptr},
        {"2-1", nullptr},
        {"2-2", nullptr},
        {"2-3", nullptr},
        {"3-1", nullptr},
        {"3-2", nullptr},
        {"3-3", nullptr}
    };

    this->_lineAlly3 = {
        {"1-1", nullptr},
        {"1-2", nullptr},
        {"1-3", nullptr},
        {"2-1", nullptr},
        {"2-2", nullptr},
        {"2-3", nullptr},
        {"3-1", nullptr},
        {"3-2", nullptr},
        {"3-3", nullptr}
    };

    this->_lineEnemy1 = {
        {"1-1", nullptr},
        {"1-2", nullptr},
        {"1-3", nullptr},
        {"2-1", nullptr},
        {"2-2", nullptr},
        {"2-3", nullptr},
        {"3-1", nullptr},
        {"3-2", nullptr},
        {"3-3", nullptr}
    };

    this->_lineEnemy2 = {
        {"1-1", nullptr},
        {"1-2", nullptr},
        {"1-3", nullptr},
        {"2-1", nullptr},
        {"2-2", nullptr},
        {"2-3", nullptr},
        {"3-1", nullptr},
        {"3-2", nullptr},
        {"3-3", nullptr}
    };

    this->_lineEnemy3 = {
        {"1-1", nullptr},
        {"1-2", nullptr},
        {"1-3", nullptr},
        {"2-1", nullptr},
        {"2-2", nullptr},
        {"2-3", nullptr},
        {"3-1", nullptr},
        {"3-2", nullptr},
        {"3-3", nullptr}
    };

    this->InitializeBattle(nbEnnemies);
}

Battle::~Battle() {

}

void Battle::InitializeBattle(unsigned int nbEnnemies) {
    std::list<Entity*>::iterator it;
    std::vector<Character*>::iterator itChars;
    std::vector<Enemy*>::iterator itEnemies;
    
    for (unsigned int i = 0; i < nbEnnemies; i++) {
        this->_enemies.push_back(new Enemy("Enemy", "Enemy"));
    }
    for (itChars = this->_characters.begin(); itChars != this->_characters.end(); itChars++) {
        this->_turns.push_back((*itChars));
    }
    for (itEnemies = this->_enemies.begin(); itEnemies != this->_enemies.end(); itEnemies++) {
        this->_turns.push_back((*itEnemies));
    }
    this->_turns.sort([](Entity*& f, Entity*& s) { return (f->GetSPD() > s->GetSPD()); });
    for (itChars = this->_characters.begin(); itChars != this->_characters.end(); itChars++) {
        switch ((*itChars)->GetLine()) {
            case 1:
                this->_lineAlly1[(*itChars)->GetIDPosition()] = (*itChars);
                break;
            case 2:
                this->_lineAlly2[(*itChars)->GetIDPosition()] = (*itChars);
                break;
            case 3:
                this->_lineAlly3[(*itChars)->GetIDPosition()] = (*itChars);
                break;
        }
    }
    for (itEnemies = this->_enemies.begin(); itEnemies != this->_enemies.end(); itEnemies++) {
        switch ((*itEnemies)->GetLine()) {
            case 1:
                this->_lineAlly1[(*itEnemies)->GetIDPosition()] = (*itEnemies);
                break;
            case 2:
                this->_lineAlly2[(*itEnemies)->GetIDPosition()] = (*itEnemies);
                break;
            case 3:
                this->_lineAlly3[(*itEnemies)->GetIDPosition()] = (*itEnemies);
                break;
        }
    }
}
