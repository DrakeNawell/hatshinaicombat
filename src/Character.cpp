#include "Character.hpp"

using namespace Game;

Character::Character(std::string const& name, std::string const& bio, std::string const&) : Entity(name, bio) {
    this->_selectedJob = 0;
    std::cout << "Character " + name + " has been created with LVL " << this->GetLVL() << std::endl;
}

Character::~Character() {

}

std::vector<Job*>& Character::GetJobs() {
    return (this->_characterJobs);
}

Job *Character::GetJob() const {
    return (this->_characterJobs[this->_selectedJob]);
}

void Character::SwitchJob(std::string const& newJob) {
    bool jobFound;

    jobFound = false;

    for (unsigned int i = 0; i < this->_characterJobs.size(); i++) {
        if (this->_characterJobs[i]->GetJobRealName().compare(newJob) == 0) {
            this->_selectedJob = i;
            jobFound = true;
        }
    }

    if (jobFound) {
        this->SetSTR(this->_characterJobs[this->_selectedJob]->GetSTR());
        this->SetPWR(this->_characterJobs[this->_selectedJob]->GetPWR());
        this->SetWSD(this->_characterJobs[this->_selectedJob]->GetWSD());
        this->SetSKL(this->_characterJobs[this->_selectedJob]->GetSKL());
        this->SetHLT(this->_characterJobs[this->_selectedJob]->GetHLT());
        this->SetSPD(this->_characterJobs[this->_selectedJob]->GetSPD());
        this->SetCST(this->_characterJobs[this->_selectedJob]->GetCST());
        this->SetSPT(this->_characterJobs[this->_selectedJob]->GetSPT());
        this->SetENG(this->_characterJobs[this->_selectedJob]->GetENG());
    } else {
        std::cout << "Selected job could not be found. Sticking to previous choice" << std::endl;
    }
}

void Character::Attack(Entity *toAttack) {
    toAttack->TakeDamage(this->GetSTR());
}

void Character::TakeDamage(unsigned int damage) {
    this->SetHP(this->GetHP() - damage);
}

std::string const Character::ToString() {
    std::string s;
    int j;

    j = 0;

    s = "";
    s += "Name : " + this->GetName() + "\n";
    s += "Bio : " + this->GetBio() + "\n";
    s += "Classe : " + this->_characterJobs[this->_selectedJob]->GetJobName() + " \n";
    s += "HP : " + std::to_string(this->GetHP()) + "\t" + "STR : " + std::to_string(this->GetSTR()) + "\n";
    s += "EP : " + std::to_string(this->GetEP()) + "\t\t" + "PWR : " + std::to_string(this->GetPWR()) + "\n";
    s += "\t\tWSD : " + std::to_string(this->GetWSD()) + "\n";
    s += "\t\tSKL : " + std::to_string(this->GetSKL()) + "\n";
    s += "\t\tHLT : " + std::to_string(this->GetHLT()) + "\n";
    s += "\t\tSPD : " + std::to_string(this->GetSPD()) + "\n";
    s += "\t\tCST : " + std::to_string(this->GetCST()) + "\n";
    s += "\t\tSPT : " + std::to_string(this->GetSPT()) + "\n";
    s += "\t\tENG : " + std::to_string(this->GetENG()) + "\n";

    s += "Status modification : \n\n";

    for (unsigned int i = 0; i < this->GetStatus().size(); i++) {
        if (this->GetStatus().at((Status)i) > 0) {
            s += StatusToString((Status)i) + " : " + std::to_string(this->GetStatus().at((Status)i)) + " stack(s)\t";
            j++;
        }
        if ((j % 3) == 0 && j != 0) {
            s += "\n";
        }
    }
    s += "\n";

    return (s);
}