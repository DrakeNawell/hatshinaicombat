#include "Characters/Aelia.hpp"

using namespace Game;

Aelia::Aelia() : Character("Aelia", "Main Character", "Analyst") {
    this->InitializeJobs();
    this->SwitchJob("Analyst");
    this->SetLine(1);
    this->SetIDPosition("1-3");
}

Aelia::~Aelia() {

}

void Aelia::InitializeJobs() {
    this->GetJobs().push_back(new Analyst("Radar Ultime", 125, 167, 110, 199, 135, 147, 142, 180, 100));
    this->GetJobs().push_back(new Healer("Thérapie par les Ondes", 101, 143, 185, 170, 147, 138, 131, 178, 100));
    this->GetJobs().push_back(new Devastator("Massacre des Ondes", 153, 199, 109, 174, 158, 128, 137, 158, 100));
}

void Aelia::InitializeSpells() {
    
}