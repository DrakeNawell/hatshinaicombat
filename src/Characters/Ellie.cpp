#include "Characters/Ellie.hpp"

using namespace Game;

Ellie::Ellie() : Character("Ellie", "Main Character", "Tank") {
    this->InitializeJobs();
    this->SwitchJob("Tank");
    this->SetLine(1);
    this->SetIDPosition("1-2");
}

Ellie::~Ellie() {

}

void Ellie::InitializeJobs() {
    this->GetJobs().push_back(new Mage("Aquamancienne", 113, 196, 142, 149, 137, 144, 162, 158, 100));
    this->GetJobs().push_back(new Tank("Gardienne Elementaire", 150, 114, 149, 129, 174, 186, 182, 125, 100));
    this->GetJobs().push_back(new Warrior("Soldat de l'Hiver", 194, 125, 117, 162, 155, 152, 121, 163, 100));
}

void Ellie::InitializeSpells() {

}