#include "Characters/Etan.hpp"

using namespace Game;

Etan::Etan() : Character("Etan", "Main character", "Healer") {
    this->InitializeJobs();
    this->SwitchJob("Healer");
    this->SetLine(1);
    this->SetIDPosition("1-1");
}

Etan::~Etan() {

}

void Etan::InitializeJobs() {
    this->GetJobs().push_back(new Gunner("Arsenal du Temps", 180, 169, 156, 184, 140, 129, 107, 156, 100));
    this->GetJobs().push_back(new Healer("Oubli des Blessures", 111, 168, 191, 152, 150, 109, 179, 121, 100));
    this->GetJobs().push_back(new Support("Maître du Temps", 107, 163, 141, 167, 152, 137, 121, 192, 100));
}

void Etan::InitializeSpells() {

}