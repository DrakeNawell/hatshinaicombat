#include "Enemy.hpp"

using namespace Game;

Enemy::Enemy(std::string const& name, std::string const& bio) : Entity(name, bio, 15) {
    this->SetHP(10000);
    this->_isAnalyzed = false;
    this->SetLine(1);
    this->SetIDPosition("1-1");
    std::cout << "Enemy " << name << " created with LVL : " << this->GetLVL() << std::endl;
}

Enemy::~Enemy() {

}

void Enemy::Attack(Entity *toAttack) {
    toAttack->TakeDamage(this->GetSTR());
}

void Enemy::TakeDamage(unsigned int damage) {
    this->SetHP(this->GetHP() - damage);
}

void Enemy::SetIsAnalyzed(bool newIsAnalyzed) {
    this->_isAnalyzed = newIsAnalyzed;
}

std::string const Enemy::ToString() {
    std::string s;
    int j;

    j = 0;
    s = "";
    s += "Name : " + this->GetName() + "\n";
    //s += "Is Analyzed : " + ((this->_isAnalyzed) ? "Yes" : "No") + "\n";
    s += "HP : " + ((this->_isAnalyzed) ? std::to_string(this->GetHP()) : "???") + "\t\t" + "ATK : " + ((this->_isAnalyzed) ? std::to_string(this->GetSTR()) : "???") + "\n";
    s += "EP : " + ((this->_isAnalyzed) ? std::to_string(this->GetEP()) : "???") + "\t\t" + "DEF : " + ((this->_isAnalyzed) ? std::to_string(this->GetCST()) : "???") + "\n";

    for (unsigned int i = 0; i < this->GetStatus().size(); i++) {
        if (this->GetStatus().at((Status)i) > 0) {
            s += StatusToString((Status)i) + " : " + std::to_string(this->GetStatus().at((Status)i)) + " stack(s)\t";
            j++;
        }
        if ((j % 3) == 0 && j != 0) {
            s += "\n";
        }
    }
    s += "\n";

    return (s);
}