#include "Entity.hpp"

using namespace Game;

Entity::Entity(std::string const& name, std::string const& bio, int lvl) {

    this->_name = name;
    this->_bio = bio;
    this->_hp = 100;
    this->_ep = 10;
    this->_exp = 0;
    this->_lvl = lvl;

    this->_str = 0;
    this->_pwr = 0;
    this->_wsd = 0;
    this->_skl = 0;
    this->_hlt = 0;
    this->_spd = 0;
    this->_cst = 0;
    this->_spt = 0;
    this->_eng = 0;

    this->_status = {
        {Status::ADRENALINE, 0},
        {Status::AGAINST_NATURE, 0},
        {Status::AGUA, 0},
        {Status::ALIVE, 0},
        {Status::ANALYZEDD, 0},
        {Status::ANTICIPATION, 0},
        {Status::ARMORED, 0},
        {Status::AROUND_THE_RIVERBEND, 0},
        {Status::ASSAULTED, 0},
        {Status::BAD_LUCK, 0},
        {Status::BAD_TREATMENT, 0},
        {Status::BLINDED, 0},
        {Status::BULLETPROOF, 0},
        {Status::CANALISER, 0},
        {Status::CARNAGE, 0},
        {Status::CLUELESS, 0},
        {Status::CLUMSINESS, 0},
        {Status::COLD, 0},
        {Status::COLD_BLOOD, 0},
        {Status::COLD_II, 0},
        {Status::COLD_III, 0},
        {Status::COMBATIVITY, 0},
        {Status::CONVALESCENCE, 0},
        {Status::COVER, 0},
        {Status::COVERED, 0},
        {Status::COWARDNESS, 0},
        {Status::CRIPPLED, 0},
        {Status::CRYO, 0},
        {Status::CURABLE, 0},
        {Status::DEAF, 0},
        {Status::DEFENSE_STANCE, 0},
        {Status::DESTROYER, 0},
        {Status::DEXTERITY, 0},
        {Status::DISTANCE_IMMUNITY, 0},
        {Status::DISTANCE_RESISTANCE, 0},
        {Status::DISTANCE_VULNERABILITY, 0},
        {Status::DISTORTION, 0},
        {Status::DISTORTION_II, 0},
        {Status::DISTURBED, 0},
        {Status::DOUBLE_CAST, 0},
        {Status::DUMB, 0},
        {Status::DYING, 0},
        {Status::FEROCIOUSNESS, 0},
        {Status::FIREY, 0},
        {Status::FROST_BITE, 0},
        {Status::GALORE, 0},
        {Status::HOLY_CLOCK, 0},
        {Status::HOURGLASS_PARADOX, 0},
        {Status::ICE_BASH, 0},
        {Status::ICE_SHIELD, 0},
        {Status::ICY_RAGE, 0},
        {Status::INSTANT_RECOVERY, 0},
        {Status::INTERCEPTION, 0},
        {Status::INVULNERABILITY, 0},
        {Status::IRON_WILL, 0},
        {Status::KO, 0},
        {Status::LAST_RESORT, 0},
        {Status::LAST_RESORT_II, 0},
        {Status::LITERATE, 0},
        {Status::LUCK, 0},
        {Status::MAGICAL_IMMUNITY, 0},
        {Status::MAGICAL_RESISTANCE, 0},
        {Status::MAGICAL_VULNERABILITY, 0},
        {Status::MAGNETISM, 0},
        {Status::MAGNETISM_II, 0},
        {Status::MELEE_IMMUNITY, 0},
        {Status::MELEE_RESISTANCE, 0},
        {Status::MELEE_VULNERABILITY, 0},
        {Status::MONO_WAYV, 0},
        {Status::OVERDRAWNED, 0},
        {Status::OVERPOWERED, 0},
        {Status::PACIFIST, 0},
        {Status::PARALYSIS, 0},
        {Status::PERFECT_SHIELD, 0},
        {Status::PHYSICAL_IMMUNITY, 0},
        {Status::PHYSICAL_RESISTANCE, 0},
        {Status::PHYSICAL_VULNERABILITY, 0},
        {Status::PLURI_WAYV, 0},
        {Status::POWER, 0},
        {Status::POWER_HEAL, 0},
        {Status::PRECISE, 0},
        {Status::PREPARED_ATTACK, 0},
        {Status::PROPHECY, 0},
        {Status::PROVOKED, 0},
        {Status::RECOVERY_STANCE, 0},
        {Status::REGENERATION, 0},
        {Status::RESISTANCE, 0},
        {Status::SCHOLAR, 0},
        {Status::SHATTERING_ICE, 0},
        {Status::SHIELD_SPIKES, 0},
        {Status::SHIELDED, 0},
        {Status::SKILLFULL, 0},
        {Status::SLOW_DOWN, 0},
        {Status::SOOTHING_WAVE, 0},
        {Status::SPEED_UP, 0},
        {Status::STATUS_RESISTANCE, 0},
        {Status::STOICISM, 0},
        {Status::STUNNED, 0},
        {Status::SUBDUED, 0},
        {Status::TAC_BURN, 0},
        {Status::TAC_TAC, 0},
        {Status::TARGET_DIAL, 0},
        {Status::THERAPY, 0},
        {Status::TIC_BURN, 0},
        {Status::TIC_TIC, 0},
        {Status::TIME_BUBBLE, 0},
        {Status::TIME_JUMP, 0},
        {Status::TIME_LAG, 0},
        {Status::TIME_SWAP, 0},
        {Status::TIME_TICKING, 0},
        {Status::TIRED, 0},
        {Status::UNABLED, 0},
        {Status::UNCURABLE, 0},
        {Status::VIBRANT, 0},
        {Status::VIOLENCE, 0},
        {Status::VITAH_LITEE, 0},
        {Status::VULNERABILITY, 0},
        {Status::WATER_SHIELD, 0},
        {Status::WEAK_HEAL, 0},
        {Status::WEAKNESS, 0},
        {Status::WET, 0},
        {Status::WET_II, 0},
        {Status::WET_III, 0},
        {Status::WILL_OF_THE_FIGHT, 0},
        {Status::WINTER_COMET, 0},
    };

}

Entity::~Entity() {

}

std::string const& Entity::GetName() const {
    return (this->_name);
}

std::string const& Entity::GetBio() const {
    return (this->_bio);
}

int Entity::GetHP() const {
    return (this->_hp);
}

int Entity::GetEP() const {
    return (this->_ep);
}

int Entity::GetLVL() const {
    return (this->_lvl);
}

int Entity::GetEXP() const {
    return (this->_exp);
}

int Entity::GetSTR() const {
    return (this->_str);
}

int Entity::GetPWR() const {
    return (this->_pwr);
}

int Entity::GetWSD() const {
    return (this->_wsd);
}

int Entity::GetSKL() const {
    return (this->_skl);
}

int Entity::GetHLT() const {
    return (this->_hlt);
}

int Entity::GetSPD() const {
    return (this->_spd);
}

int Entity::GetCST() const {
    return (this->_cst);
}

int Entity::GetSPT() const {
    return (this->_spt);
}

int Entity::GetENG() const {
    return (this->_eng);
}

std::map<Status, char> const& Entity::GetStatus() const {
    return (this->_status);
}

int Entity::GetLine() const {
    return (this->_line);
}

std::string const& Entity::GetIDPosition() const {
    return (this->_idPos);
}

void Entity::SetName(std::string const& newName) {
    this->_name = newName;
}

void Entity::SetBio(std::string const& newBio) {
    this->_bio = newBio;
}

void Entity::SetHP(int newHP) {
    this->_hp = newHP;
}

void Entity::SetEP(int newEP) {
    this->_ep = newEP;
}

void Entity::SetLVL(int newLVL) {
    this->_lvl = newLVL;
}

void Entity::SetEXP(int newEXP) {
    this->_exp = newEXP;
}

void Entity::SetSTR(int newSTR) {
    this->_str = newSTR;
}

void Entity::SetPWR(int newPWR) {
    this->_pwr = newPWR;
}

void Entity::SetWSD(int newWSD) {
    this->_wsd = newWSD;
}

void Entity::SetSKL(int newSKL) {
    this->_skl = newSKL;
}

void Entity::SetHLT(int newHLT) {
    this->_hlt = newHLT;
}

void Entity::SetSPD(int newSPD) {
    this->_spd = newSPD;
}

void Entity::SetCST(int newCST) {
    this->_cst = newCST;
}

void Entity::SetSPT(int newSPT) {
    this->_spt = newSPT;
}

void Entity::SetENG(int newENG) {
    this->_eng = newENG;
}

void Entity::SetLine(int newLine) {
    this->_line = newLine;
}

void Entity::SetIDPosition(std::string const& newIDPosition) {
    this->_idPos = newIDPosition;
}

void Entity::ApplyStatus(Status toApply, StatusType type) {
    switch (type) {
        case StatusType::ONE_SHOT:
            this->_status[toApply] = 1;
            break;
        case StatusType::STACK:
            this->_status[toApply]++;
            break;
    }
}

void Entity::DismissStatus(Status toDismiss) {
    this->_status[toDismiss] = 0;
}

unsigned int Entity::GetStatusStack(Status toGet) {
    return (this->_status[toGet]);
}