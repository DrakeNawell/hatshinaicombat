#include "Job.hpp"

using namespace Game;

Job::Job(std::string const& jobName, std::string const& realName) {

    this->_jobName = jobName;
    this->_jobRealName = realName;

    this->_str = 0;
    this->_pwr = 0;
    this->_wsd = 0;
    this->_skl = 0;
    this->_hlt = 0;
    this->_spd = 0;
    this->_cst = 0;
    this->_spt = 0;
    this->_eng = 0;

    this->_resWTR = 0;
    this->_resFIR = 0;
    this->_resGRD = 0;
    this->_resAIR = 0;
    this->_resTND = 0;
    this->_resICE = 0;
    this->_resNTR = 0;
    this->_resMCH = 0;
    this->_resLGT = 0;
    this->_resDRK = 0;
    this->_resPHS = 0;
    this->_resMGK = 0;
    this->_resWAV = 0;
    this->_resSND = 0;

}

Job::Job(std::string const& jobName, std::string const& realName, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng) {

    this->_jobName = jobName;
    this->_jobRealName = realName;

    this->_str = str;
    this->_pwr = pwr;
    this->_wsd = wsd;
    this->_skl = skl;
    this->_hlt = hlt;
    this->_spd = spd;
    this->_cst = cst;
    this->_spt = spt;
    this->_eng = eng;

    this->_resWTR = 0;
    this->_resFIR = 0;
    this->_resGRD = 0;
    this->_resAIR = 0;
    this->_resTND = 0;
    this->_resICE = 0;
    this->_resNTR = 0;
    this->_resMCH = 0;
    this->_resLGT = 0;
    this->_resDRK = 0;
    this->_resPHS = 0;
    this->_resMGK = 0;
    this->_resWAV = 0;
    this->_resSND = 0;

}

Job::Job(std::string const& jobName, std::string const& realName, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng,
        int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND) {

    this->_jobName = jobName;
    this->_jobRealName = realName;

    this->_str = str;
    this->_pwr = pwr;
    this->_wsd = wsd;
    this->_skl = skl;
    this->_hlt = hlt;
    this->_spd = spd;
    this->_cst = cst;
    this->_spt = spt;
    this->_eng = eng;

    this->_resWTR = resWTR;
    this->_resFIR = resFIR;
    this->_resGRD = resGRD;
    this->_resAIR = resAIR;
    this->_resTND = resTND;
    this->_resICE = resICE;
    this->_resNTR = resNTR;
    this->_resMCH = resMCH;
    this->_resLGT = resLGT;
    this->_resDRK = resDRK;
    this->_resPHS = resPHS;
    this->_resMGK = resMGK;
    this->_resWAV = resWAV;
    this->_resSND = resSND;

}

Job::~Job() {

}

std::string const& Job::GetJobName() const {
    return (this->_jobName);
}

std::string const& Job::GetJobRealName() const {
    return (this->_jobRealName);
}

int Job::GetSTR() const {
    return (this->_str);
}

int Job::GetPWR() const {
    return (this->_pwr);
}

int Job::GetWSD() const {
    return (this->_wsd);
}

int Job::GetSKL() const {
    return (this->_skl);
}

int Job::GetHLT() const {
    return (this->_hlt);
}

int Job::GetSPD() const {
    return (this->_spd);
}

int Job::GetCST() const {
    return (this->_cst);
}

int Job::GetSPT() const {
    return (this->_spt);
}

int Job::GetENG() const {
    return (this->_eng);
}

int Job::GetResWTR() const {
    return (this->_resWTR);
}

int Job::GetResFIR() const {
    return (this->_resFIR);
}

int Job::GetResGRD() const {
    return (this->_resGRD);
}

int Job::GetResAIR() const {
    return (this->_resAIR);
}

int Job::GetResTND() const {
    return (this->_resTND);
}

int Job::GetResICE() const {
    return (this->_resICE);
}

int Job::GetResNTR() const {
    return (this->_resNTR);
}

int Job::GetResMCH() const {
    return (this->_resMCH);
}

int Job::GetResLGT() const {
    return (this->_resLGT);
}

int Job::GetResDRK() const {
    return (this->_resDRK);
}

int Job::GetResPHS() const {
    return (this->_resPHS);
}

int Job::GetResMGK() const {
    return (this->_resMGK);
}

int Job::GetResWAV() const {
    return (this->_resWAV);
}

int Job::GetResSND() const {
    return (this->_resSND);
}

void Job::SetSTR(int newSTR) {
    this->_str = newSTR;
}

void Job::SetPWR(int newPWR) {
    this->_pwr = newPWR;
}

void Job::SetWSD(int newWSD) {
    this->_wsd = newWSD;
}

void Job::SetSKL(int newSKL) {
    this->_skl = newSKL;
}

void Job::SetHLT(int newHLT) {
    this->_hlt = newHLT;
}

void Job::SetSPD(int newSPD) {
    this->_spd = newSPD;
}

void Job::SetCST(int newCST) {
    this->_cst = newCST;
}

void Job::SetSPT(int newSPT) {
    this->_spt = newSPT;
}

void Job::SetENG(int newENG) {
    this->_eng = newENG;
}

void Job::SetResWTR(int newResWTR) {
    this->_resWTR = newResWTR;
}

void Job::SetResFIR(int newResFIR) {
    this->_resFIR = newResFIR;
}

void Job::SetResGRD(int newResGRD) {
    this->_resGRD = newResGRD;
}

void Job::SetResAIR(int newResAIR) {
    this->_resAIR = newResAIR;
}

void Job::SetResTND(int newResTND) {
    this->_resTND = newResTND;
}

void Job::SetResICE(int newResICE) {
    this->_resICE = newResICE;
}

void Job::SetResNTR(int newResNTR) {
    this->_resNTR = newResNTR;
}

void Job::SetResMCH(int newResMCH) {
    this->_resMCH = newResMCH;
}

void Job::SetResLGT(int newResLGT) {
    this->_resLGT = newResLGT;
}

void Job::SetResDRK(int newResDRK) {
    this->_resDRK = newResDRK;
}

void Job::SetResPHS(int newResPHS) {
    this->_resPHS = newResPHS;
}

void Job::SetResMGK(int newResMGK) {
    this->_resMGK = newResMGK;
}

void Job::SetResWAV(int newResWAV) {
    this->_resWAV = newResWAV;
}

void Job::SetResSND(int newResSND) {
    this->_resSND = newResSND;
}

std::vector<Spell*> const& Job::GetSpells() const {
    return (this->_jobSpells);
}
