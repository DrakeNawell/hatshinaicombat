#include "Jobs/Analyst.hpp"

using namespace Game;

Analyst::Analyst(std::string const& jobName) : Job(jobName, "Analyst") {

}

Analyst::Analyst(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng)
    : Job(name, "Analyst", str, pwr, wsd, skl, hlt, spd, cst, spt, eng) {

}

Analyst::Analyst(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng, int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND) 
        : Job(name, "Analyst", str, pwr, wsd, skl, hlt, spd, cst, spt, eng, resWTR, resFIR, resGRD, resAIR, resTND, resICE, resNTR, resMCH,
                resLGT, resDRK, resPHS, resMGK, resWAV, resSND) {

}


Analyst::~Analyst() {
    
}