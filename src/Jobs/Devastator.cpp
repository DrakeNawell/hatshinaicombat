#include "Jobs/Devastator.hpp"

using namespace Game;

Devastator::Devastator(std::string const& jobName) : Job(jobName, "Devastator") {

}

Devastator::Devastator(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng)
    : Job(name, "Devastator", str, pwr, wsd, skl, hlt, spd, cst, spt, eng) {

}

Devastator::Devastator(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng, int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND) 
        : Job(name, "Devastator", str, pwr, wsd, skl, hlt, spd, cst, spt, eng, resWTR, resFIR, resGRD, resAIR, resTND, resICE, resNTR, resMCH,
                resLGT, resDRK, resPHS, resMGK, resWAV, resSND) {

}

Devastator::~Devastator() {
    
}