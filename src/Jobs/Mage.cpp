#include "Jobs/Mage.hpp"

using namespace Game;

Mage::Mage(std::string const& jobName) : Job(jobName, "Mage") {

}

Mage::Mage(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng)
    : Job(name, "Mage", str, pwr, wsd, skl, hlt, spd, cst, spt, eng) {

}

Mage::Mage(std::string const& name, int str, int pwr, int wsd, int skl, int hlt, int spd, int cst, int spt, int eng, int resWTR, int resFIR, int resGRD, int resAIR, int resTND, int resICE, int resNTR, int resMCH, int resLGT, int resDRK, int resPHS, int resMGK, int resWAV, int resSND) 
        : Job(name, "Mage", str, pwr, wsd, skl, hlt, spd, cst, spt, eng, resWTR, resFIR, resGRD, resAIR, resTND, resICE, resNTR, resMCH,
                resLGT, resDRK, resPHS, resMGK, resWAV, resSND) {

}

Mage::~Mage() {
    
}