#include "Spell.hpp"

using namespace Game;

Spell::Spell(std::string const& name) {
    this->_name = name;
    this->_energyUsed = 0;
    this->_criticalChances = 0;
    this->_precision = 0;
    this->_pwrSpellMax = 0;
    this->_pwrSpellMin = 0;
}

Spell::~Spell() {

}

unsigned int Spell::GetChance(int, int max) {
    return (this->_rd() % max);
}

std::string const& Spell::GetName() const {
    return (this->_name);
}

unsigned int Spell::GetEnergyUsed() const {
    return (this->_energyUsed);
}

double Spell::GetCriticalChances() const {
    return (this->_criticalChances);
}

unsigned int Spell::GetPrecision() const {
    return (this->_precision);
}

unsigned int Spell::GetPWRSpellMin() const {
    return (this->_pwrSpellMin);
}

unsigned int Spell::GetPWRSpellMax() const {
    return (this->_pwrSpellMax);
}

void Spell::SetName(std::string const& newName) {
    this->_name = newName;
}

void Spell::SetEnergyUsed(int newEnergyUsed) {
    this->_energyUsed = newEnergyUsed;
}

void Spell::SetCriticalChances(int newCriticalChances) {
    this->_criticalChances = newCriticalChances;
}

void Spell::SetPrecision(int newPrecision) {
    this->_precision = newPrecision;
}

void Spell::SetPWRSpellMin(int newPWRSpellMin) {
    this->_pwrSpellMin = newPWRSpellMin;
}

void Spell::SetPWRSpellMax(int newPWRSpellMax) {
    this->_pwrSpellMax = newPWRSpellMax;
}