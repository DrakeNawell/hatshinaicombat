#include "Status.hpp"

std::string const StatusToString(Status toString) {
    std::string s;

    switch (toString) {
        case Status::ADRENALINE:
            s = "Adrenaline";
            break;
        case Status::TIRED:
            s = "Tired";
            break;
        case Status::OVERPOWERED:
            s = "Overpowered";
            break;
        case Status::DISTURBED:
            s = "Disturbed";
            break;
        case Status::LITERATE:
            s = "Literate";
            break;
        case Status::DUMB:
            s = "Dumb";
            break;
        case Status::DEXTERITY:
            s = "Dexterity";
            break;
        case Status::CLUMSINESS:
            s = "Clumsiness";
            break;
        case Status::ALIVE:
            s = "Alive";
            break;
        case Status::DYING:
            s = "Dying";
            break;
        case Status::SPEED_UP:
            s = "Speed Up";
            break;
        case Status::SLOW_DOWN:
            s = "Slow Down";
            break;
        case Status::ARMORED:
            s = "Armored";
            break;
        case Status::ASSAULTED:
            s = "Assaulted";
            break;
        case Status::COVERED:
            s = "Covered";
            break;
        case Status::CANALISER:
            s = "Canaliser";
            break;
        case Status::COMBATIVITY:
            s = "Combativity";
            break;
        case Status::WEAKNESS:
            s = "Weakness";
            break;
        case Status::RESISTANCE:
            s = "Resistance";
            break;
        case Status::VULNERABILITY:
            s = "Vulnerability";
            break;
        case Status::VIOLENCE:
            s = "Violence";
            break;
        case Status::PACIFIST:
            s = "Pacifist";
            break;
        case Status::POWER:
            s = "Power";
            break;
        case Status::UNABLED:
            s = "Unabled";
            break;
        case Status::FEROCIOUSNESS:
            s = "Ferociousness";
            break;
        case Status::COWARDNESS:
            s = "Cowardness";
            break;
        case Status::CARNAGE:
            s = "Carnage";
            break;
        case Status::CRIPPLED:
            s = "Crippled";
            break;
        case Status::PHYSICAL_RESISTANCE:
            s = "Physical Resistance";
            break;
        case Status::PHYSICAL_VULNERABILITY:
            s = "Physical Vulnerability";
            break;
        case Status::MAGICAL_RESISTANCE:
            s = "Magical Resistance";
            break;
        case Status::MAGICAL_VULNERABILITY:
            s = "Magical Vulnerability";
            break;
        case Status::MELEE_RESISTANCE:
            s = "Melee Resistance";
            break;
        case Status::MELEE_VULNERABILITY:
            s = "Melee Vulnerability";
            break;
        case Status::DISTANCE_RESISTANCE:
            s = "Distance Resistance";
            break;
        case Status::DISTANCE_VULNERABILITY:
            s = "Distance Vulnerability";
            break;
        case Status::CURABLE:
            s = "Curable";
            break;
        case Status::UNCURABLE:
            s = "Uncurable";
            break;
        case Status::SCHOLAR:
            s = "Scholar";
            break;
        case Status::CLUELESS:
            s = "Clueless";
            break;
        case Status::PRECISE:
            s = "Precise";
            break;
        case Status::BLINDED:
            s = "Blinded";
            break;
        case Status::SKILLFULL:
            s = "Skillfull";
            break;
        case Status::STOICISM:
            s = "Stoicism";
            break;
        case Status::LUCK:
            s = "Luck";
            break;
        case Status::BAD_LUCK:
            s = "Bad Luck";
            break;
        case Status::DESTROYER:
            s = "Destroyer";
            break;
        case Status::SUBDUED:
            s = "Subdued";
            break;
        case Status::BULLETPROOF:
            s = "Bulletproof";
            break;
        case Status::OVERDRAWNED:
            s = "Overdrawned";
            break;
        case Status::INTERCEPTION:
            s = "Interception";
            break;
        case Status::INVULNERABILITY:
            s = "Invulnerability";
            break;
        case Status::PHYSICAL_IMMUNITY:
            s = "Physical Immunity";
            break;
        case Status::MAGICAL_IMMUNITY:
            s = "Magical Immunity";
            break;
        case Status::MELEE_IMMUNITY:
            s = "Melee Immunity";
            break;
        case Status::DISTANCE_IMMUNITY:
            s = "Distance Immunity";
            break;
        case Status::WILL_OF_THE_FIGHT:
            s = "Will of the Fight";
            break;
        case Status::WET:
            s = "Wet";
            break;
        case Status::COLD:
            s = "Cold";
            break;
        case Status::AROUND_THE_RIVERBEND:
            s = "Around the Riverbend";
            break;
        case Status::CRYO:
            s = "Cryo";
            break;
        case Status::AGUA:
            s = "Agua";
            break;
        case Status::GALORE:
            s = "Galore";
            break;
        case Status::WET_II:
            s = "Wet II";
            break;
        case Status::COLD_II:
            s = "Cold II";
            break;
        case Status::WET_III:
            s = "Wet III";
            break;
        case Status::COLD_III:
            s = "Cold III";
            break;
        case Status::PROVOKED:
            s = "Provoked";
            break;
        case Status::IRON_WILL:
            s = "Iron Will";
            break;
        case Status::STUNNED:
            s = "Stunned";
            break;
        case Status::WATER_SHIELD:
            s = "Water Shield";
            break;
        case Status::ICE_SHIELD:
            s = "Ice Shield";
            break;
        case Status::DEFENSE_STANCE:
            s = "Defense Stance";
            break;
        case Status::SOOTHING_WAVE:
            s = "Shooting Wave";
            break;
        case Status::SHIELD_SPIKES:
            s = "Shield Spikes";
            break;
        case Status::FROST_BITE:
            s = "Frost Bite";
            break;
        case Status::LAST_RESORT:
            s = "Last Resort";
            break;
        case Status::LAST_RESORT_II:
            s = "Last Resort II";
            break;
        case Status::COLD_BLOOD:
            s = "Cold Blood";
            break;
        case Status::PREPARED_ATTACK:
            s = "Prepared Attack";
            break;
        case Status::PARALYSIS:
            s = "Paralysis";
            break;
        case Status::ICE_BASH:
            s = "Ice Bash";
            break;
        case Status::SHATTERING_ICE:
            s = "Shattering Ice";
            break;
        case Status::ICY_RAGE:
            s = "Icy Rage";
            break;
        case Status::WINTER_COMET:
            s = "Winter Comet";
            break;
        case Status::TIC_BURN:
            s = "Tic Burn";
            break;
        case Status::TIME_TICKING:
            s = "Time Ticking";
            break;
        case Status::TIME_LAG:
            s = "Time Lag";
            break;
        case Status::FIREY:
            s = "Firey";
            break;
        case Status::HOURGLASS_PARADOX:
            s = "Hourglass Paradox";
            break;
        case Status::TARGET_DIAL:
            s = "Target Dial";
            break;
        case Status::DOUBLE_CAST:
            s = "Double Cast";
            break;
        case Status::TIME_JUMP:
            s = "Time Jump";
            break;
        case Status::TAC_TAC:
            s = "Tac Tac";
            break;
        case Status::TIC_TIC:
            s = "Tic Tic";
            break;
        case Status::TAC_BURN:
            s = "Tac Burn";
            break;
        case Status::HOLY_CLOCK:
            s = "Holy Clock";
            break;
        case Status::KO:
            s = "KO";
            break;
        case Status::CONVALESCENCE:
            s = "Convalescence";
            break;
        case Status::TIME_SWAP:
            s = "Time Swap";
            break;
        case Status::INSTANT_RECOVERY:
            s = "Instant Recovery";
            break;
        case Status::PROPHECY:
            s = "Prophecy";
            break;
        case Status::REGENERATION:
            s = "Regeneration";
            break;
        case Status::RECOVERY_STANCE:
            s = "Recovery Stance";
            break;
        case Status::POWER_HEAL:
            s = "Power Heal";
            break;
        case Status::AGAINST_NATURE:
            s = "Against Nature";
            break;
        case Status::SHIELDED:
            s = "Shielded";
            break;
        case Status::WEAK_HEAL:
            s = "Weak Heal";
            break;
        case Status::DISTORTION:
            s = "Distortion";
            break;
        case Status::TIME_BUBBLE:
            s = "Time Bubble";
            break;
        case Status::DISTORTION_II:
            s = "Distortion II";
            break;
        case Status::ANTICIPATION:
            s = "Anticipation";
            break;
        case Status::COVER:
            s = "Cover";
            break;
        case Status::VITAH_LITEE:
            s = "Vitah Litee";
            break;
        case Status::MONO_WAYV:
            s = "Mono Wayv";
            break;
        case Status::PLURI_WAYV:
            s = "Pluri Wayv";
            break;
        case Status::ANALYZEDD:
            s = "ANALYZEDD";
            break;
        case Status::VIBRANT:
            s = "Vibrant";
            break;
        case Status::DEAF:
            s = "Deaf";
            break;
        case Status::THERAPY:
            s = "Therapy";
            break;
        case Status::STATUS_RESISTANCE:
            s = "Status Resistance";
            break;
        case Status::MAGNETISM:
            s = "Magnetism";
            break;
        case Status::MAGNETISM_II:
            s = "Magnetism II";
            break;
        case Status::BAD_TREATMENT:
            s = "Bad Treatment";
            break;
        case Status::PERFECT_SHIELD:
            s = "Perfect Shield";
            break;
    }
    return (s);
}